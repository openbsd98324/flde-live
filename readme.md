# FLDE  -  Fast Light Desktop 

Image for AMD64.

Login: netbsd 

password: ??? <-- what is the best of BSD ?

(solution: netbsd)

![](media/846d02fe49264c1b9805cdd4da02e7e8.png)
(flde screenshot, which is running on Raspberry PI)




## Installation to USB pendrive (or as well on harddisk)

**1.) Download**

You can download several version, the barebone (initial release), or flde stable or testing.


1.1.) Initial release (barebone, with slack, linux devuan, netbsd, freebsd. flde runs on sdb4 (msdos4))

wget under Linux, BSD,...
````
wget -c --no-check-certificate https://gitlab.com/openbsd98324/flde-live/-/raw/main/pub/flde-live/bsd-linux/v1.2/flde-live-memstick-image-1655222827-net-v1.2-1-x86_64_amd64-1b.img.gz 
````


[Direct link](https://gitlab.com/openbsd98324/flde-live/-/raw/main/pub/flde-live/bsd-linux/v1.2/flde-live-memstick-image-1655222827-net-v1.2-1-x86_64_amd64-1b.img.gz)


or from current: 

1.2.) Current release

See below in the section "Current Release".
There are:  release "Stable" and release "Testing".
Stable is more reliable, testing offers new features. 


**2.) Prepare the memstick**

Use zcat (usual command in Linux and BSD)

````
zcat flde-live-memstick-image-1655222827-net-v1.2-1-x86_64_amd64-1b.img.gz > /dev/sdb 

````


If you use Windows, you may use rawrite for Windows. 
Link: https://gitlab.com/openbsd98324/flde-live/-/raw/main/pub/rawrite/rawrite-win-1.0.9.0.zip
![](pub/rawrite/rawrite.png)




**3.) Boot**

Press F9, or F12 (... see bios of PC)

Select Memstick/USB disk

Select in Grub: e.g. USB-Pendrive to sdb4 for the OS Linux, with FLDE Live
sdb4 is the partition with flde live (fltk).

## Boot Loader

Grub is installed on first partition (netbsd ffs). 
The first partition has a live Slackware (EFI) Live/toram, as a rescue, which can be started from Grub.

- If you installed on USB Pendrive, you may select boot from sdb4.

- If you installed on main harddisk, you may select boot from sda4.

- For mmc/sd, it is mmc (SD), i.e. mmcblk0p4.


## Current Releases 

**Stable**

Direct link: https://gitlab.com/openbsd98324/flde-live/-/raw/main/pub/current/flde-live-stable.img.gz

The classic FLDE.

linphone with alsa, gnuplot.

**Testing**

Direct link: https://gitlab.com/openbsd98324/flde-live/-/raw/main/pub/current/flde-live-testing.img.gz

 The FLDE-live testing release offers **linphone and pulseaudio**  for SIP phone calls.
 It has pulseaudio (devuan) to have higher compatibility with mics/speakers.
 flsetting has been added, which offers more configuration utils. 
 flgnuplot is included. 
It included Web TV and Web radio (streams).

Linphone is available for web telephony (web phone calls, VOIP).
 
![](media/linphone.png)

 
![](media/pavucontrol.png)



 
![](media/fltvstream.png)



 
![](media/fltvstream1.png)


 
![](media/fltvstream2.png)

 
![](media/flstreamtuner.png)


## Source code

https://gitlab.com/openbsd98324/flde


## License 

GNU - Free Software



